
// Load the SDK for JavaScript
const AWS = require('aws-sdk');
const async = require('async');
//const Cobuild = require('cobuild2');
const baseModel = require('aurora-mongo-query');

//services (only availables in aurora-ds)
//const anteService = Cobuild.Utils.Files.requireIfExists('/app/services/ante.js');
//const logService = Cobuild.Utils.Files.requireIfExists('/app/modules/cobuild/logs/services/logService');

let queueUrl;
let sqs;

module.exports.init = function init(config) {
  queueUrl = config.queueUrl;
  // Set the region 
  AWS.config.update({ region: config.region });

  // Create an SQS service object
  sqs = new AWS.SQS({ apiVersion: config.apiVersion });
}

module.exports.receiveMessages = function receiveMessages() {
  return new Promise(async (resolve, reject) => {
    const elements = await baseModel.get('element', { 'schemaData.technology': 'ante' })
    if (!elements || !elements.length) {
      return resolve([])
    }
    async.reduce(elements, [], (results, element, cb) => {
      //parse elementId
      const elementId = element.elementId.toLowerCase().replace(/\W\s*/g, '_');

      //params for reading messages
      const params = {
        MaxNumberOfMessages: 1,
        QueueUrl: queueUrl + elementId,
        VisibilityTimeout: 0,
        WaitTimeSeconds: 0,
        MessageAttributeNames: [
          'nodeAddress',
          'ttl',
          'priority'
        ]
      };
      console.log(queueUrl + elementId);
      results.push(new Promise((resolve, reject) => {
        sqs.receiveMessage(params, (err, data) => {
          if (err) {
            console.log("Receive Error ", queueUrl + elementId, err);
            return reject(err);
          } else if (data.Messages) {
            const deleteParams = {
              QueueUrl: queueUrl + elementId,
              ReceiptHandle: data.Messages[0].ReceiptHandle
            };
            sqs.deleteMessage(deleteParams, (err, dataDelete) => {
              if (err) {
                console.log("Delete Error", err);
                return reject(err);
              } else {
                console.log("Message Deleted", dataDelete);
                return resolve(data.Messages[0])
              }
            });
          } else {
            return resolve(null)
          }
        });
      }))
      return cb(null, results)
    }, async function (err, listPromise) {
      const results = await Promise.all(listPromise)
      if (err) {
        console.log("error reading messages");
        return reject(err)
      }
      console.log("sucess reading messages");
      return resolve(results.filter((data) => data))
    }
    );
  });
};

module.exports.getMessagesFromQueue = function getMessagesFromQueue(queueId) {
  return new Promise((resolve, reject) => {
    const params = {
      MaxNumberOfMessages: 10,
      QueueUrl: queueUrl + queueId,
      VisibilityTimeout: 10,
      WaitTimeSeconds: 0,
      MessageAttributeNames: [
        'nodeAddress',
        'ttl',
        'priority'
      ]
    };
    console.log(queueUrl + queueId);
    sqs.receiveMessage(params, async (err, data) => {
      if (err) {
        console.log("Receive Error ", queueUrl + queueId, err);
        return reject(err)
      } else if (data.Messages && data.Messages.length) {
        const messages = data.Messages;
        const arrayDeletes = [];

        //delete messages
        messages.forEach((message) => {
          arrayDeletes.push(removeQueueMessage(queueId, message));
        });
        await Promise.all(arrayDeletes)
        return resolve(messages);
      } else {
        return resolve([]);
      }
    });
  });
}

function removeQueueMessage(queueId, message) {
  return new Promise((resolve, reject) => {
    const deleteParams = {
      QueueUrl: queueUrl + queueId,
      ReceiptHandle: message.ReceiptHandle
    };
    sqs.deleteMessage(deleteParams, function (err, data) {
      if (err) {
        console.log("Delete Error", err);
        return reject(err);
      } else {
        console.log("Message Deleted", data);
        return resolve(data);
      }
    });
  });
}

module.exports.sendMessages = function sendMessages(elementId, frame, nodeAddress, priority, ttl, delay) {

  return new Promise((resolve, reject) => {
    elementId = elementId.toLowerCase();
    elementId = elementId.replace(/\W\s*/g, '_');

    const params = {
      DelaySeconds: delay,
      MessageAttributes: {
        nodeAddress: {
          DataType: "String",
          StringValue: nodeAddress + ""
        },
        priority: {
          DataType: "Number",
          StringValue: priority + ""
        },
        ttl: {
          DataType: "Number",
          StringValue: ttl + ""
        }
      },
      MessageBody: frame,
      QueueUrl: queueUrl + elementId
    };

    sqs.sendMessage(params, function (err, data) {
      if (err) {
        console.log("Error", err);
        return reject(err);
      } else {
        console.log("Success", data.MessageId, " | ", delay);
        return resolve(data.MessageId);
      }
    });
  });
};

module.exports.createQueues = function createQueues() {
  return new Promise((resolve, reject) => {
    baseModel.getAll('elements')
      .then(elements => {
        async.each(elements, element => {
          if (element.isActive && element.schemaData.technology == 'ante')
            this.createQueue(element.elementId);
        }, err => {
          if (err) {
            reject(err);
          } else {
            resolve();
          }
        });
      });
  })
}

module.exports.createQueue = function createQueue(name) {
  return new Promise((resolve, reject) => {
    name = name.toLowerCase();
    name = name.replace(/\W\s*/g, '_');
    const params = {
      QueueName: name
    };
    sqs.createQueue(params, function (err, data) {
      if (err) {
        reject(err);
        console.log(err, err.stack); // an error occurred
      } else {
        resolve(data);
        console.log(data);           // successful response
      }
    });
  });
}

module.exports.deleteQueue = function deleteQueue(name) {
  return new Promise((resolve, reject) => {
    name = name.toLowerCase();
    name = name.replace(/\W\s*/g, '_');
    const params = {
      QueueUrl: queueUrl + name
    };

    sqs.deleteQueue(params, function (err, data) {
      if (err) {
        reject(err);
        console.log(err, err.stack); // an error occurred
      } else {
        resolve(data);
        console.log(data);           // successful response
      }
    });
  });
}