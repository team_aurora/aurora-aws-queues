# aurora-aws-queues Package #


## Objective ##

**aurora-aws-queues package** provides utilities for AWS-SQS.

## Installation ##

### Usage ###

* Install the package
```
#!javascript
npm install https://bitbucket.org/team_aurora/aurora-aws-queues.git --save
```

* Require it in your code
```
#!javascript
const awsQueue = require('aurora-aws-queues');
```